package SocketPackage;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Derin Anggara on 11/9/2017.
 */
public class ServerSocketHandler {
    private int portnumber;
    private ServerSocket serverSocket = null;
    private Socket clientSocket = null;
    private PrintWriter printStream = null;
    private BufferedReader dataInputStream = null;
    private String line = "";

    public ServerSocketHandler(int portnumber) {
        this.portnumber = portnumber;
    }

    public void OpenSocketClient() {
        System.out.println("Open Server Socket");
        try {
            serverSocket = new ServerSocket(portnumber);
            System.out.println("Server Socket Openned");
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    public void OpenService() {
        System.out.println("Listening To: " + portnumber);
        try
        {
            while (true){
                System.out.println("Ready!");
                clientSocket = serverSocket.accept();
                new Server(clientSocket).start();
            }
//            while (true){
//                System.out.println("Still Listening To: " + portnumber);
//                line = dataInputStream.readLine();
//                printStream.print("Ok" + "\r\n");
//            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    private class Server extends Thread {
        Socket socket;

        Server(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            super.run();
            PrintWriter printStreamThread = null;
            BufferedReader dataInputStreamThread = null;
            try {
                dataInputStreamThread = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                printStreamThread = new PrintWriter(socket.getOutputStream(), true);
                System.out.println("Accepting: " + portnumber);
                String textFromClient = dataInputStreamThread.readLine();
                System.out.println(textFromClient);
                printStreamThread.println("Ok");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
