package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import SocketPackage.ClientSocketHandler;

import java.net.Socket;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("Login.fxml"));
        primaryStage.setTitle("Healthmation");
        primaryStage.setScene(new Scene(root, 375, 239));
        primaryStage.setResizable(false);
        primaryStage.show();

        Thread clientThread = new Thread(new Client());
        clientThread.start();


    }

    public static class Client implements Runnable{

        @Override
        public void run() {
            int portnumber = 9999;
            String host = "192.168.43.241";

            ClientSocketHandler clientSocketHandler = new ClientSocketHandler(host,portnumber);
            clientSocketHandler.OpenSocketClient();
            String response = clientSocketHandler.ConnectToServer("get_all_user#;");
            System.out.println(response);
        }

    }

    public static void main(String[] args) {
        launch(args);
    }
}
